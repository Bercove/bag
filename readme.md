### BAG REST API Documentation

This repository contains the source code for BAG's REST API, facilitating user management and authentication. The API allows users to perform various actions, including signing up, signing in, signing out, and retrieving user information. Below is a breakdown of key components and features provided by the API.

#### Key Components

1. **User Management:** The API provides endpoints for creating user accounts, retrieving user information, and updating user details.

2. **Authentication:** Users can sign in to the platform using their email and password. The API generates access tokens and refresh tokens for authenticated users.

3. **Token Management:** The API supports token-based authentication using JSON Web Tokens (JWT). Access tokens are issued upon successful authentication and can be used to access protected endpoints. Refresh tokens are provided for obtaining new access tokens when they expire.

4. **Error Handling:** The API includes error handling mechanisms to provide clear and informative responses to client requests. Errors such as invalid credentials or server errors are appropriately handled and communicated to the client.

#### Setup Instructions

To run the API locally, follow these steps:

1. Clone the repository to your local machine.
2. Install the required dependencies using `pip install -r requirements.txt`.
3. Configure the database settings in the `settings.py` file.
4. Apply database migrations using `python manage.py migrate`.
5. Start the development server with `python manage.py runserver`.

#### API Endpoints

- `/api/v1/auth/users/`: GET endpoint to retrieve a list of all users.
- `/api/v1/auth/users/create-staff`: POST endpoint to create a new staff user account.
- `/api/v1/auth/auth/sign-in`: POST endpoint for user authentication and obtaining access tokens.
- `/api/v1/auth/auth/sign-in/refresh`: POST endpoint to refresh access tokens.
- `/api/v1/auth/auth/sign-up`: POST endpoint for user registration.
- `/api/v1/auth/auth/sign-out`: POST endpoint for user logout.

For detailed information on request and response formats, refer to the source code and docstrings provided in each module.

#### Contributing

Contributions to the development of this API are welcome. If you encounter any issues or have suggestions for improvements, please submit a pull request or open an issue on GitHub.

#### License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.
