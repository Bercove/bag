from celery import shared_task
from eHuuza.celery import app # ! Don't remove this line 
from django_celery_results.models import TaskResult

from Default.models import whoisIPData, Subscribers, UserData
from helpers.mail_notification import *

@shared_task(bind=True)
def createWhoisData(self):
    batch_size = 50
    total_records = UserData.objects.count()

    for offset in range(0, total_records, batch_size):
        queryset = UserData.objects.all()[offset:offset+batch_size]
        for query in queryset:
            task_id = self.request.id
            task_name = 'createWhoisData'

            try:
                whoisIPData.create_from_whois(query.ip)
                status = 'SUCCESS'
            except Exception as e:
                status = 'FAILURE'
                raise e
              
            update_task_result(task_id, task_name, status)

@shared_task(bind=True)
def sendNewsEventsEmail(self, title, url, intro, cover, body, createdAt, modelName):
    batch_size = 50
    total_records = Subscribers.objects.count()

    for offset in range(0, total_records, batch_size):
        subscriber_emails = Subscribers.objects.all().values_list('email', flat=True)[offset:offset+batch_size]
        for subscriber_email in subscriber_emails:
            task_id = self.request.id
            task_name = 'sendNewsEventsEmail'

            try:
                sendNewsEventsEmailToSubscribers(title, url, intro, cover, body, createdAt, modelName, subscriber_email)
                status = 'SUCCESS'
            except Exception as e:
                status = 'FAILURE'
                raise e
              
            update_task_result(task_id, task_name, status)

@shared_task(bind=True)
def sendSubscriptionEmail(self, email, userName):
    task_id = self.request.id
    task_name = 'sendSubscriptionEmail'

    try:
        subscription(email, userName)
        status = 'SUCCESS'
    except Exception as e:
        status = 'FAILURE'
        raise e
      
    update_task_result(task_id, task_name, status)

def update_task_result(task_id, task_name, status):
    task_result, created = TaskResult.objects.get_or_create(
        task_id=task_id,
        defaults={'task_name': task_name, 'status': status}
    )
