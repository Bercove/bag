WEEK_DAY = (
    ('Monday', 'Monday'),
    ('Tuesday', 'Tuesday'),
    ('Wednesday', 'Wednesday'),
    ('Thursday', 'Thursday'),
    ('Friday', 'Friday'),
    ('Saturday', 'Saturday'),
    ('Sunday', 'Sunday')
)

PRIORITY_CHOICE = (
    ('Low', 'Low'),
    ('Medium', 'Medium'),
    ('High', 'High'),
)

CASE_CHOICE = (
    ('New', 'New'),
    ('In Progress', 'In Progress'),
    ('Closed', 'Closed'),
)
