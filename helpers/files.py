import os, uuid, base64, six, imghdr
from django.utils.deconstruct import deconstructible
from django.core.files.base import ContentFile
from rest_framework import serializers    
from imagekit.admin import AdminThumbnail
from imagekit import ImageSpec
from imagekit.processors import ResizeToFill
from imagekit.cachefiles import ImageCacheFile

@deconstructible
class RandomFileName(object):
    def __init__(self, path):
        self.path = os.path.join(path, "%s%s")

    def __call__(self, _, filename):
        # @note It's up to the validators to check if it's the correct file type in name or if one even exist.
        extension = os.path.splitext(filename)[1]
        return self.path %("%s%s"%('e-huuza-', uuid.uuid4()), extension)

def isValidUUID(string):
    try:
        uuid_obj = uuid.UUID(str(string))
        return True
    except ValueError:
        return False

class Base64ImageField(serializers.ImageField):
    """
    A Django REST framework field for handling image-uploads through raw post data.
    It uses base64 for encoding and decoding the contents of the file.

    Updated for Django REST framework 3.
    """

    def to_internal_value(self, data):

        # Check if this is a base64 string
        if isinstance(data, six.string_types):
            # Check if the base64 string is in the "data:" format
            if 'data:' in data and ';base64,' in data:
                # Break out the header from the base64 content
                header, data = data.split(';base64,')

            # Try to decode the file. Return validation error if it fails.
            try:
                decoded_file = base64.b64decode(data)
            except TypeError:
                self.fail('invalid_image')

            # Generate file name:
            file_name = str(uuid.uuid4())[:12] # 12 characters are more than enough.
            # Get the file name extension:
            file_extension = self.get_file_extension(file_name, decoded_file)

            complete_file_name = "%s.%s" % (file_name, file_extension, )

            data = ContentFile(decoded_file, name=complete_file_name)

        return super(Base64ImageField, self).to_internal_value(data)

    def get_file_extension(self, file_name, decoded_file):
        extension = imghdr.what(file_name, decoded_file)
        extension = "jpg" if extension == "jpeg" else extension

        return extension
    
class AdminThumbnailSpec(ImageSpec):
    if ImageSpec is not None:
        processors = [ResizeToFill(100, 50)]
        format = 'JPEG'
        Options = {'quality': 60}

def cached_admin_thumbCover(instance):
    try:
        if instance.cover and os.path.exists(instance.cover.path):
            cached = ImageCacheFile(AdminThumbnailSpec(instance.cover))
            cached.generate()
            return cached
    except FileNotFoundError:
        pass
    return None

def ThumbnailCover():
    return AdminThumbnail(image_field=cached_admin_thumbCover)
