from django.http import Http404
from rest_framework.response import Response
from rest_framework.pagination import PageNumberPagination

class StandardResultsSetPagination(PageNumberPagination):
    page_size_query_param = 'page_size'
 
    def paginate_queryset(self, queryset, request, view=None):
        requested_page_size = request.query_params.get('page_size')
        
        if requested_page_size == '-1':
            self.page_size = queryset.count()
        else:
            try:
                if requested_page_size is not None:
                    self.page_size = int(requested_page_size)
                    if self.page_size < -1:
                        raise Http404("Invalid page size. Please provide a valid page size.")
                else:
                    self.page_size = 10
            except ValueError:
                raise Http404("Invalid page size. Please provide a valid page size.")
        
        return super().paginate_queryset(queryset, request, view)

    def get_paginated_response(self, data):
        if self.page_size == -1:
            total_pages = 1
        else:
            total_pages = self.page.paginator.num_pages

        return Response({
            'page_size': self.page_size,
            'total_objects': self.page.paginator.count,
            'total_pages': total_pages,
            'current_page': self.page.number,
            'next': self.get_next_link(),
            'previous': self.get_previous_link(),
            'results': data,
        })
