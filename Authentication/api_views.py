import random
from django.db.models import Q
from rest_framework import generics, status
from rest_framework.response import Response
from rest_framework.views import APIView
from django.contrib.auth import authenticate, login
from rest_framework.authtoken.models import Token
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework_simplejwt.views import TokenObtainPairView
from rest_framework_simplejwt.token_blacklist.models import OutstandingToken, BlacklistedToken
from rest_framework_simplejwt.tokens import RefreshToken
from rest_framework_simplejwt.authentication import JWTAuthentication

from Authentication.models import *
from Authentication.serializers import *
from templatetags.customFilter import *

class UsersView(generics.ListAPIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]
    serializer_class = UserSerializer

    def get_queryset(self, *args, **kwargs):
        return Response(User.objects.all()).data

class AuthenticationSignUp(generics.ListAPIView):
    
    def post(self, request, *args, **kwargs):
        serializer = UserSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            user = User.objects.get(username=serializer.data['username'])
            refresh = RefreshToken.for_user(user)
            res = {
                "accessToken": str(refresh.access_token),
                "refreshToken": str(refresh),
                "user": UserSerializer(user, context={"request": request}).data,
            }
            return Response(res, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    
class SignOut(APIView):
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        if self.request.data.get('all'):
            token: OutstandingToken
            for token in OutstandingToken.objects.filter(user=request.user):
                _, _ = BlacklistedToken.objects.get_or_create(token=token)
            return Response({"status": "OK, goodbye, all refresh tokens blacklisted"})
        refresh_token = self.request.data.get('refresh_token')
        token = RefreshToken(token=refresh_token)
        token.blacklist()
        return Response({"status": "OK, goodbye"})

class AuthenticationSignIn(TokenObtainPairView):

    def post(self, request):
        email = request.data.get("email")
        password = request.data.get("password")
        user = authenticate(email=email, password=password)
        if user:
            login(request, user)
            if user.is_active:
                refresh = RefreshToken.for_user(user)
                res = {
                    "accessToken": str(refresh.access_token),
                    "refreshToken": str(refresh),
                    "user": UserSerializer(user, context={"request": request}).data,
                }
                return Response(res, status=status.HTTP_200_OK)
            else:
                return Response(
                    {   
                        "err": "Unauthorized Request", 
                        "message": "User is not yet approved"
                    }, status=status.HTTP_401_UNAUTHORIZED
                )
        else:
            return Response(
                {    
                    "message": "Please check your email and password if they are correct."
                }, status=status.HTTP_400_BAD_REQUEST
            )

class CreateUserAccount(generics.ListAPIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]
    
    def post(self, request, *args, **kwargs):
        print(request.data)
        serializer = WriteUserSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            user = User.objects.get(username=serializer.data['username'])
            refresh = RefreshToken.for_user(user)
            res = {
                "accessToken": str(refresh.access_token),
                "refreshToken": str(refresh),
                "user": UserSerializer(user, context={"request": request}).data,
            }
            return Response(res, status=status.HTTP_201_CREATED)
        print(serializer.errors)
        return Response({    
                    "message": serializer.errors
                }, status=status.HTTP_400_BAD_REQUEST)