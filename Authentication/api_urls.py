from django.urls import path
from django.conf import settings
from django.conf.urls.static import static
from rest_framework_simplejwt.views import TokenRefreshView

from Authentication import api_views as view

urlpatterns = [
    path('users/', view.UsersView.as_view(), name='user'),
    path('users/create-staff', view.CreateUserAccount.as_view(), name='create-user-account'),
    path('auth/sign-in', view.AuthenticationSignIn.as_view(), name='sign-in'),
    path('auth/sign-in/refresh', TokenRefreshView.as_view(), name='refresh-token'),
    path('auth/sign-up', view.AuthenticationSignUp.as_view(), name='sign-up'),
    path('auth/sign-out', view.SignOut.as_view(), name='sign-out'),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
