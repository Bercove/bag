from django.apps import AppConfig


class AuthenticationConfig(AppConfig):
    name = 'Authentication'
    verbose_name = 'Authentication'
    verbose_name_plural = 'Authentications'
