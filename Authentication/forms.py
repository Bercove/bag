from django import forms

from Authentication.models import User

class UserForm(forms.ModelForm):

    class Meta:
        model = User
        fields = ('first_name', 'middle_name', 'last_name', 'email', 'phone')
        widgets = {
            "first_name":forms.TextInput(attrs={"required": True}),
            "middle_name":forms.TextInput(attrs={"required": False}),
            "last_name":forms.TextInput(attrs={"required": True}),
            "email":forms.EmailInput(attrs={"required": False}),
            'phone_number':forms.TextInput(
                attrs={
                    "required": True,
                    "type":"tel",
                }
            )
        }
