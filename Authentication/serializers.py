from rest_framework import serializers

from Authentication.models import *
from templatetags.customFilter import phoneNumber

class PhoneNumberField(serializers.Field):
    def to_representation(self, value):
        return f"{phoneNumber(value)}"

class UserSerializer(serializers.ModelSerializer):
    profile_picture = serializers.FileField(required=False, allow_null=True)
    phone = PhoneNumberField()
    
    class Meta:
        model = User
        fields = ('id', 'first_name', 'middle_name', 'last_name', 'email', 'phone', 'category', 'profile_picture', )
        read_only_fields = ['id']
        
    def get_profile_picture(self, obj):
        request = self.context.get('request')
        if obj.profile_picture:
            return request.build_absolute_uri(obj.profile_picture.url)
        return None

class WriteUserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('first_name', 'middle_name', 'last_name', 'email', 'phone',)
        