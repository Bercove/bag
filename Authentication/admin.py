from django.contrib import admin
from django.urls import path, reverse
from django.forms import NumberInput
from django.utils.html import strip_tags, format_html
from django.template.loader import render_to_string
from django.core.mail import EmailMultiAlternatives
from django.db import models
from django.contrib.auth.admin import UserAdmin
from django.utils.translation import gettext_lazy as _
from django.shortcuts import HttpResponse, get_object_or_404, render
from django.contrib.contenttypes.models import ContentType
from django_admin_logs.admin import LogEntryAdmin as BaseLogEntryAdmin
from django.contrib.admin.models import LogEntry, ADDITION, CHANGE, DELETION

from django.contrib.auth.models import Permission
from django.contrib.contenttypes.models import ContentType

from Authentication.models import *
from Authentication import views as view

admin.site.unregister(LogEntry)

@admin.register(User)
class UserAdmin(UserAdmin):
    list_display = ("first_name", "middle_name", "last_name", "email", "phone", "is_active", "category")

    # ordering = ("first_name", "middle_name", "last_name", "email", "phone", "is_active")
    ordering = ("-date_joined", )
    search_fields = ("first_name", "middle_name", "last_name", "email", "phone")

    list_filter = []
    
    fieldsets = ()
    
    add_fieldsets =(
        (None, {
            'classes': ('wide',),
            'fields': ('first_name', 'middle_name', 'last_name', 'email', 'phone', 'password1', 'password2')}
        ),
    )
    
    formfield_overrides = {
        models.IntegerField: {
            'widget': NumberInput(
                attrs = {
                    "size": 200
                }
            )
        }
    }

    formfield_overrides = {
        models.PositiveBigIntegerField: {
            'widget': NumberInput(
                attrs={
                    'size': 100
                }
            )
        }
    }
    
    def get_urls(self):
        urls = super().get_urls()
        custom_urls = [
            path('<int:user>/generate-confirmation-email/', self.generateConfirmationEmail, name='generate-confirmation-email'),
            path("profile/", self.admin_site.admin_view(self.profile), name="user-profile")
        ]
        return custom_urls + urls        
    
    def get_queryset(self, request):
        
        return super().get_queryset(request)
    
    def profile(self, request):
        template_name = "admin/api/user/profile.html"
        model = User
        user = get_object_or_404(User, id=request.user.id)
        
        context = dict(
            self.admin_site.each_context(request),
            cl = self.get_changelist_instance(request),
            opts = model._meta,
            title = "My Profile",
            add = self.has_add_permission(request),
            change = self.has_change_permission(request),
            app_label = "Authentication",
            user = user,
            has_add_permission = self.has_add_permission(request),
            app_list = self.admin_site.get_app_list(request),
        )
        
        return render(request, template_name=template_name, context=context)

    def add_view(self, *args, **kwargs):
        self.inlines = []
        return super(UserAdmin, self).add_view(*args, **kwargs)
    
    # def formfield_for_choice_field(self, db_field, request, **kwargs):
    #     if db_field.name == "category" and not request.user.is_superuser:
    #         kwargs['choices'] = (("aggregator", "AGGREGATOR"),("sales-agent", "SALES AGENT"),)
    #     return super(UserAdmin, self).formfield_for_choice_field(db_field, request, **kwargs)

    # def formfield_for_manytomany(self, db_field, request, **kwargs):
    #     if db_field.name == "groups" and request.user.category != 'coordinator' and not request.user.is_superuser and 'territory' in request.user.category:
    #         kwargs['queryset'] = Group.objects.filter(name="aggregator")
    #     return super().formfield_for_manytomany(db_field, request, **kwargs)

    # def change_view(self, request, object_id, *args, **kwargs):
    #     if request.user.category == 'internal-territory':
            
    #         self.fieldsets = (
    #             (None, {'fields': ('first_name', 'last_name', 'phone', 'email', 'password', 'category')}),
    #             (_('Permissions'), {'fields': ('is_active', 'is_staff',
    #                                         'groups', )}),
    #             (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    #         )
    #     else:
    #         self.fieldsets = (
    #             (None, {'fields': ('first_name', 'last_name', 'phone', 'email', 'password', ('category', 'status'))}),
    #             (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser', 
    #                                         'groups', 'user_permissions')}),
    #             (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    #         )
    #     return super(UserAdmin, self).change_view(request, object_id, *args, **kwargs)
    
    def generateConfirmationEmail(self, request, user):
        user = User.objects.filter(id=user).first()
        context = dict(
           # Include common variables for rendering the admin template.
           self.admin_site.each_context(request),
           # Anything else you want in the context...
           user=user,
        )
        
        subject = "Email Confirmation | Musanze Campus"
        html_message = render_to_string('admin/authentication/confirm.html', context)

        text_content = strip_tags(html_message)
        email = "musanze@uok.ac.rw"
        msg = EmailMultiAlternatives(subject, text_content, "norepley@warmme.com", [email])
        msg.attach_alternative(html_message, "text/html")
        msg.send()
        
        return HttpResponse("message sent!")

@admin.register(CustomLogEntry)
class CustomLogEntryAdmin(BaseLogEntryAdmin):
    # list_display = ('action_time', 'user', 'content_type', 'object_repr', 'action_flag', 'change_message')
    # search_fields = ('user__username', 'object_repr', 'change_message')
    # list_display = ('action_time', 'user', 'action', 'content_type', 'object')
    search_fields = ()
    date_hierarchy = 'action_time'
    list_filter = ('action_flag', 'content_type')
    list_select_related = ('user',)
    ordering = ('-action_time',)
    readonly_fields = ('action_time', 'user', 'content_type', 'object_id', 'object_repr', 'action_flag', 'change_message')
    fields = ('action_time', 'user', 'content_type', 'object_id', 'object_repr', 'action_flag', 'change_message')

    def get_queryset(self, request, *args, **kwargs):
        if request.user.is_superuser:
            queryset = LogEntry.objects.filter().all()
        else:
            queryset = LogEntry.objects.filter(user=request.user.id)
        return queryset
    
    def get_list_filter(self, request):
        list_filter = super().get_list_filter(request)
        if not request.user.is_superuser:
            list_filter = ('action_flag',)
        return list_filter

    # def get_list_filter(self, request):
    #     if request.user.is_superuser:
    #         return ('action_flag', 'content_type')
    #     else:
    #         user_permissions = Permission.objects.filter(user=request.user)
    #         content_types = ContentType.objects.filter(
    #             model__in=[permission.content_type.model for permission in user_permissions]
    #         )
    #         content_type_ids = [content_type.id for content_type in content_types]
    #         return ('action_flag', 'content_type__id__in', content_type_ids)

