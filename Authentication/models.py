from django.contrib import auth
from django.contrib.auth.models import  PermissionsMixin
from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager
from django.core.mail import send_mail
from django.db import models
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from django.utils.encoding import smart_str
from django.contrib.admin.models import LogEntry, ADDITION, CHANGE, DELETION
from rest_framework.authtoken.models import Token

from helpers.files import RandomFileName

class UserManager(BaseUserManager):
    use_in_migrations = True

    def _create_user(self, first_name, last_name, email, password, **extra_fields):
        """
        Create and save a user with the given email and password.
        """
        if not email:
            raise ValueError('The given email must be set')
        email = self.normalize_email(email)
        first_name = first_name
        last_name = self.last_name
        user = self.model(first_name, last_name,email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, first_name, last_name, email=None, password=None, **extra_fields):
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(first_name, last_name, email, password, **extra_fields)

    def create_superuser(self, first_name, last_name, email=None, password=None, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(first_name, last_name, email, password, **extra_fields)

    def with_perm(self, perm, is_active=True, include_superusers=True, backend=None, obj=None):
        if backend is None:
            backends = auth._get_backends(return_tuples=True)
            if len(backends) == 1:
                backend, _ = backends[0]
            else:
                raise ValueError(
                    'You have multiple authentication backends configured and '
                    'therefore must provide the `backend` argument.'
                )
        elif not isinstance(backend, str):
            raise TypeError(
                'backend must be a dotted import path string (got %r).'
                % backend
            )
        else:
            backend = auth.load_backend(backend)
        if hasattr(backend, 'with_perm'):
            return backend.with_perm(
                perm,
                is_active=is_active,
                include_superusers=include_superusers,
                obj=obj,
            )
        return self.none()

class AbstractUser(AbstractBaseUser, PermissionsMixin):
    """
    An abstract base class implementing a fully featured User model with
    admin-compliant permissions.

    Username and password are required. Other fields are optional.
    """
    category_choice = (
        ('Staff', 'Staff'),
        ('User', 'User')
    )
    first_name = models.CharField(_('First Name'), max_length=30, null=False, blank=False)
    middle_name = models.CharField(_('Middle Name'), max_length=30, null=True, blank=True)
    last_name = models.CharField(_('Last Name'), max_length=150, null=False, blank=False)
    email = models.EmailField(_('E-mail'), unique=True)
    phone = models.PositiveBigIntegerField(_("Phone"), unique=True, null=True, blank=True)
    is_staff = models.BooleanField(
        _('Can login'),
        default=False,
        help_text=_("Designates whether the user can connect to this Dashboard."),
    )
    is_active = models.BooleanField(
        _('active'),
        default=False,
        help_text=_(
            'Indicates whether this user should be treated as active. '
            'Deselect this instead of deleting accounts.'
        ),
    )
    category = models.CharField(_("Category"), max_length=12, choices=category_choice, null=True, blank=False)
    profile_picture = models.ImageField(_("Profile Picture"), upload_to=RandomFileName('users/profile/'), default='images/user-avator-blue.png', max_length=255)
    date_joined = models.DateTimeField(_("Date joined"), default=timezone.now)

    objects = UserManager()

    EMAIL_FIELD = 'email'
    USERNAME_FIELD = 'email'
    # REQUIRED_FIELDS = ['email']

    class Meta:
        verbose_name = _('user')
        verbose_name_plural = _('users')
        abstract = True
    
    def __str__(self):
        if self.middle_name is None:
            return f"{self.first_name} {self.last_name}"
        else:
            return f"{self.first_name} {self.middle_name} {self.last_name}"

    def clean(self):
        super().clean()
        self.email = self.__class__.objects.normalize_email(self.email)

    @property
    def full_name(self):
        """
        Return the first_name plus the last_name, with a space in between.
        """
        if self.middle_name is None:
            return f"{self.first_name} {self.last_name}"
        else:
            return f"{self.first_name} {self.middle_name} {self.last_name}"
    
    def get_email(self):
        """
        Return the email.
        """
        return self.email

    def get_short_name(self):
        """Return the short name for the user."""
        return self.first_name

    def email_user(self, subject, message, from_email=None, **kwargs):
        """Send an email to this user."""
        send_mail(subject, message, from_email, [self.email], **kwargs)
    
    @property
    def token(self):
        return Token.objects.get(user=self.id)

class User(AbstractUser):
    """
    Users within the Django authentication system are represented by this
    model.

    Username and password are required. Other fields are optional.
    """
    # class Meta(AbstractUser.Meta):
    #     swappable = ''AUTH_USER_MODEL

class LogEntryManager(models.Manager):
    use_in_migrations = True

    def log_action(self, user_id, content_type_id, object_id, object_repr, action_flag, change_message=''):
        e = self.model(
            user_id=user_id,
            content_type_id=content_type_id,
            object_id=smart_str(object_id),
            object_repr=object_repr[:200],
            action_flag=action_flag,
            change_message=change_message
        )
        e.save()

class CustomLogEntry(LogEntry):
    class Meta:
        proxy = True
        verbose_name = _('Log Entry')
        verbose_name_plural = _('Log Entries')

    @property
    def action(self):
        return self.get_action_flag_display()

    @property
    def object(self):
        return str(self)
    