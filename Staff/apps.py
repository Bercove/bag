from django.apps import AppConfig

class StaffConfig(AppConfig):
    name = 'Staff'
    verbose_name = 'Staff'
    verbose_name_plural = 'Staff'
