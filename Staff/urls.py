from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static
from rest_framework.routers import DefaultRouter

from Staff import views as view

router = DefaultRouter()

urlpatterns = [
    path('', view.index, name='index'),
    # path('agent-product/<int:id>/detail', AgentProductView.as_view(), name='farmer_detail'),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

urlpatterns += router.urls
