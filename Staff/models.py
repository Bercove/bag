import uuid
from django.db import models
from django.utils import timezone
from django.db.models import Q
from django.utils.html import format_html
from django.utils.translation import gettext_lazy as _
from django.template.defaultfilters import truncatechars

from imagekit.models import ImageSpecField
from imagekit.processors import ResizeToFill

from helpers.files import RandomFileName

from Authentication.models import *

# 
# Create your models here.
#   
status_choice = (
    (1, 'Approved'), 
    (2, 'Disapproved')
)

class Positions(models.Model):
    name = models.CharField(_("Position name"), max_length=255)
    created_at = models.DateTimeField(_("Created At"), default=timezone.now)
    updated_at = models.DateTimeField(_("Update At"), auto_now=True)

    class Meta:
        managed = True
        verbose_name = 'Position'
        verbose_name_plural = 'Positions'
    
    def __str__(self):
        return self.name

class Staffs(models.Model):
    user = models.ForeignKey(
        User, 
        on_delete=models.CASCADE,
        related_name='user_in_staff',
        unique=False, null=False, blank=False,
        verbose_name='User',
    )
    about = models.TextField(_("About"), null=True, blank=True)
    is_active = models.BooleanField(_("Active"), 
        help_text=_(
            'Indicates whether this user should be treated as active. '
            'Deselect this instead of deleting accounts.'
        ), default=True, null=False, blank=False)
    created_at = models.DateTimeField(_("Created At"), default=timezone.now)
    updated_at = models.DateTimeField(_("Update At"), auto_now=True)

    class Meta:
        managed = True
        ordering = ['created_at']
        verbose_name = 'Staff'
        verbose_name_plural = 'Staffs'
    
    def __str__(self):
        if self.user.middle_name is None:
            return "%s %s"%(self.user.first_name, self.user.last_name)
        else:
            return "%s %s %s"%(self.user.first_name, self.user.middle_name, self.user.last_name)

    def getStaffEmail(self):
        return self.user.email
    getStaffEmail.short_description = "Email"

    def getStaffPhone(self):
        return self.user.phone
    getStaffPhone.short_description = "Phone"

    def getStaffPosition(self):
        positions = ""
        allPositions = self.staff_in_staffpostion.filter(staff=self.id)
        for index, position in enumerate(allPositions):
            positions += position.position.name
            if index != (len(allPositions) - 1):
                positions += " & "
        return positions
    getStaffPosition.short_description = "Positions"

class StaffPositions(models.Model):
    staff = models.ForeignKey(
        "Staffs", 
        on_delete=models.CASCADE,
        related_name='staff_in_staffpostion',
        verbose_name='Staff',
        unique=False, null=False, blank=False
    )
    position = models.ForeignKey(
        "Positions", 
        on_delete=models.CASCADE,
        related_name='position_in_staffposition',
        verbose_name='Position',
        unique=False, null=False, blank=False
    )
    created_at = models.DateTimeField(_("Created At"), default=timezone.now)
    updated_at = models.DateTimeField(_("Update At"), auto_now=True)
        
    class Meta:
        managed = True
        ordering = ['created_at']
        verbose_name = 'Position'
        verbose_name_plural = 'Positions'

    def __str__(self):
        return "%s (%s)"%(self.staff,  self.position)

class Specializations(models.Model):
    staff = models.ForeignKey(
        Staffs, 
        on_delete=models.CASCADE,
        related_name='staff_in_specializations',
        verbose_name='Staff',
        unique=False, null=False, blank=False
    )
    title = models.CharField(_("Title"), max_length=255, null=False, blank=False)
    created_at = models.DateTimeField(_("Created At"), default=timezone.now)
    updated_at = models.DateTimeField(_("Update At"), auto_now=True)
        
    class Meta:
        managed = True
        ordering = ['created_at']
        verbose_name = 'Specialization'
        verbose_name_plural = 'Specializations'

    def __str__(self):
        return "%s"%self.title

