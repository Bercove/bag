from django.db.models import Q, F, Sum, Count
from rest_framework.exceptions import ValidationError
from rest_framework import mixins
from rest_framework import status
from rest_framework import generics
from rest_framework import viewsets
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.authtoken.models import Token
from django.contrib.auth.hashers import make_password
from datetime import datetime, timedelta
from django.utils import timezone
from django.shortcuts import get_object_or_404, redirect
from django.views.decorators.csrf import csrf_exempt
from django.core.exceptions import PermissionDenied
from django.contrib.auth import authenticate, login
from django.shortcuts import render, HttpResponse
from django.http import JsonResponse

from Authentication.models import *
from Staff.models import *
from Staff.serializers import *
from Staff.helpers import *

def index(request):
    return HttpResponse("Welcome to BAG API")