from django.contrib import admin
from django import forms
from django.db.models import F
from django.utils import timezone
from django.urls import path, include
from django.utils.html import format_html
from django.db.models import Count, Sum, F
from django.forms import (NumberInput)
from django.conf import settings
from django_summernote.admin import SummernoteModelAdmin
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import get_object_or_404, render, HttpResponse, redirect

from Authentication.models import *
from Staff.models import *
from Staff.forms import *

# Register your models here.

@admin.register(Positions)
class PositionsAdmin(admin.ModelAdmin):
    model = Positions
    extra = 1

    exclude = ('created_at', )

class StaffPositionsStackedInline(admin.StackedInline):
    model = StaffPositions
    extra = 0

    exclude = ('created_at', )

class SpecializationsStackedInline(admin.StackedInline):
    model = Specializations
    extra = 0

    exclude = ('created_at', )

@admin.register(Staffs)
class StaffsAdmin(SummernoteModelAdmin):
    list_display = ('__str__', 'getStaffEmail', 'getStaffPhone', 'getStaffPosition', 'is_active')
    search_fields = ()
    summernote_fields = ('about')

    exclude = ('created_at', )

    add_form = StaffForm

    inlines = [
        StaffPositionsStackedInline,
        SpecializationsStackedInline,
    ]

