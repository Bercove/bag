from urllib import request
from django.db.models import Q, F, Sum, Count
from django import forms
from django.forms.models import BaseInlineFormSet
from django_summernote.widgets import SummernoteWidget

from Authentication.models import *
from Staff.models import *

class StaffForm(forms.ModelForm):
    about = forms.CharField(widget=SummernoteWidget())

    class Meta:
        model = Staffs
        fields = ('about',)
    
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)