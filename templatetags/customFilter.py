from django import template

register = template.Library()

@register.filter
def phoneNumber(number):
    if number is not None:
        if countIntegerNumber(number) == 9:
            return f"+250{number}"
        elif countIntegerNumber(number) >= 12:
            return f"+{number}"
        else:
            return f"+{number}"
    else:
        return ""
